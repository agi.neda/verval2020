package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ExampleSignInPage {
    private String usernameField = "uernameId";
    By passwordField = By.xpath("//input[contains(@id, 'passwordId')]");

    @FindBy(className="button")  //@FindBy(name="name") // FindBy(id="uid") // FindBy(xpath="xpath")
    WebElement signInButton;

    WebDriver webDriver;

    public ExampleSignInPage(WebDriver webDriver) {
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this); //this is needed if @FindBy is used
    }

    public void writeTextToUsernameField(String text) {
        webDriver.findElement(By.id(usernameField)).sendKeys(text);
    }

    public void writeTextToPasswordField(String text) {
        webDriver.findElement(passwordField).sendKeys(text);
    }

    public void pressSignInButton() {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(signInButton)).click();
    }

    //methods using assert methods can be written to verify if everything went right
}
